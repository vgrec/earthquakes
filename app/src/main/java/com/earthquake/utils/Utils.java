package com.earthquake.utils;

import android.util.Log;

public class Utils {
    private static final String TAG = "EARTHQUAKE";

    public static void logError(String message, Throwable throwable) {
        Log.d(TAG, message, throwable);
    }

    public static void logInfo(String message) {
        Log.d(TAG, message);
    }

}
