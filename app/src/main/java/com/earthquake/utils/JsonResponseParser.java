package com.earthquake.utils;

import com.earthquake.model.Earthquake;
import com.earthquake.model.EarthquakeResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class that converts a json string to {@link EarthquakeResponse}.
 */
public class JsonResponseParser {
    private static final String KEY_EARTHQUAKES = "earthquakes";
    private static final String KEY_DATETIME = "datetime";
    private static final String KEY_DEPTH = "depth";
    private static final String KEY_MAGNITUDE = "magnitude";
    private static final String KEY_LATITUDE = "lat";
    private static final String KEY_LONGITUDE = "lng";

    /**
     * Convert the json string to {@link EarthquakeResponse}.
     */
    public static EarthquakeResponse toEarthquakeResponse(String json) {
        try {
            EarthquakeResponse response = new EarthquakeResponse();
            List<Earthquake> earthquakes = new ArrayList<>();

            JSONObject responseObject = new JSONObject(json);
            JSONArray array = responseObject.getJSONArray(KEY_EARTHQUAKES);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                earthquakes.add(toEarthQuake(object.toString()));
            }

            response.setEarthquakes(earthquakes);

            return response;
        } catch (JSONException e) {
            Utils.logError("Could not parse the json string", e);
        }

        return new EarthquakeResponse(); // empty response
    }

    /**
     * Convert a json string to {@link Earthquake}.
     */
    private static Earthquake toEarthQuake(String json) {
        try {
            Earthquake earthquake = new Earthquake();

            JSONObject object = new JSONObject(json);
            earthquake.setDate(getString(object, KEY_DATETIME));
            earthquake.setDepth(getDouble(object, KEY_DEPTH));
            earthquake.setMagnitude(getDouble(object, KEY_MAGNITUDE));
            earthquake.setLatitude(getString(object, KEY_LATITUDE));
            earthquake.setLongitude(getString(object, KEY_LONGITUDE));

            return earthquake;

        } catch (JSONException e) {
            Utils.logError("Could not parse the json string", e);
        }

        return null;
    }

    /**
     * @return the String value for the specified key or null
     * if the value could not be read.
     */
    private static String getString(JSONObject object, String key) {
        try {
            return object.getString(key);
        } catch (JSONException e) {
            Utils.logError("Could not read attribute: " + key, e);
        }

        return null;
    }

    /**
     * @return the Double value for the specified key or null
     * if the value could not be read.
     */
    private static Double getDouble(JSONObject object, String key) {
        try {
            return object.getDouble(key);
        } catch (JSONException e) {
            Utils.logError("Could not read attribute: " + key, e);
        }

        return null;
    }

}
