package com.earthquake.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class EarthquakeResponse {
    private List<Earthquake> earthquakes = new ArrayList<>();

    public List<Earthquake> getEarthquakes() {
        return earthquakes;
    }

    public void setEarthquakes(@NonNull List<Earthquake> earthquakes) {
        this.earthquakes = earthquakes;
    }
}
