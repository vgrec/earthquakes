package com.earthquake.model;

public class Earthquake {
    private String date;
    private Double depth;
    private Double magnitude;
    private String latitude;
    private String longitude;

    public String getDate() {
        return date;
    }

    public Double getDepth() {
        return depth;
    }

    public Double getMagnitude() {
        return magnitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDepth(Double depth) {
        this.depth = depth;
    }

    public void setMagnitude(Double magnitude) {
        this.magnitude = magnitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
