package com.earthquake.ui.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.earthquake.R;
import com.earthquake.model.Earthquake;

import java.util.List;

public class EarthquakeListAdapter extends BaseAdapter {
    private static final double MAGNITUDE_DANGEROUS = 8.0;

    private List<Earthquake> earthquakes;
    private EarthquakeListAdapter.Listener listener;

    public EarthquakeListAdapter(
            List<Earthquake> earthquakes,
            EarthquakeListAdapter.Listener listener
    ) {
        this.earthquakes = earthquakes;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return earthquakes.size();
    }

    @Override
    public Earthquake getItem(int position) {
        return earthquakes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list, parent, false);
            holder.magnitude = view.findViewById(R.id.magnitude);
            holder.depth = view.findViewById(R.id.depth);
            holder.dateAndTime = view.findViewById(R.id.dateAndTime);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Context context = view.getContext();
        Earthquake earthquake = earthquakes.get(position);

        holder.magnitude.setText(String.valueOf(earthquake.getMagnitude()));
        holder.depth.setText(context.getString(R.string.depth_label, earthquake.getDepth()));
        holder.dateAndTime.setText(context.getString(R.string.date_label, earthquake.getDate()));

        int viewColor = earthquake.getMagnitude() >= MAGNITUDE_DANGEROUS
                ? ContextCompat.getColor(context, R.color.color_dangerous)
                : ContextCompat.getColor(context, android.R.color.transparent);
        view.setBackgroundColor(viewColor);

        view.setOnClickListener(v -> {
            listener.onItemClicked(earthquake);
        });

        return view;
    }

    private static class ViewHolder {
        TextView magnitude;
        TextView depth;
        TextView dateAndTime;
    }

    public interface Listener {
        void onItemClicked(Earthquake earthquake);
    }
}
