package com.earthquake.ui.list;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.earthquake.http.RequestManager;
import com.earthquake.http.Response;
import com.earthquake.http.StringRequest;

/**
 * The main entry point for requesting earthquake data.
 * <p>
 * This class is a wrapper around {@link RequestManager}
 * and its main purpose is to properly handle http requests
 * in the event of a configuration change, as well as caching
 * the previous response.
 * <p>
 * Technically this is achieved by retaining the instance of the
 * Fragment across Activity re-creations via "setRetainInstance(true)",
 */
public class EarthquakeDataLoader extends Fragment {

    private static final String URL = "http://api.geonames.org/earthquakesJSON" +
            "?formatted=true" +
            "&north=44.1" +
            "&south=-9.9" +
            "&east=-22.4" +
            "&west=55.2" +
            "&username=mkoppelman";

    private RequestManager requestManager = new RequestManager();
    private StringRequest currentRequest;
    private Listener responseListener;
    private Response<String> response;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * Load earthquake data from the server or return a
     * cached response if the data was previously loaded.
     *
     * @param responseListener a listener to be invoked once a {@link Response} is available.
     */
    void loadData(Listener responseListener) {
        this.responseListener = responseListener;

        if (response != null) {
            responseListener.onResponseReady(response);
            return;
        }

        performRequest();
    }

    /**
     * Trigger a refresh of the earthquake data by skipping
     * the cached response and performing a new http request instead.
     *
     * @param responseListener a listener to be invoked once a {@link Response} is available.
     */
    void refreshData(Listener responseListener) {
        this.responseListener = responseListener;
        performRequest();
    }

    private void performRequest() {
        // Make sure we cancel the current request before issuing a new one.
        // In a general context this ensures that we disregard any results
        // from previous started requests, thus taking in consideration results
        // only from the most recent one.
        if (currentRequest != null) {
            currentRequest.cancel();
        }

        currentRequest = new StringRequest(URL, response -> {
            // "Cache" the response for later usage.
            this.response = response;

            if (responseListener != null) {
                responseListener.onResponseReady(response);
            }
        });

        requestManager.execute(currentRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        responseListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        currentRequest.cancel();
        requestManager.shutDown();
    }

    public interface Listener {
        void onResponseReady(Response<String> response);
    }

}
