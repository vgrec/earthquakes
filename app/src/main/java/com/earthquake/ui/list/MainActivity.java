package com.earthquake.ui.list;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.earthquake.R;
import com.earthquake.http.Response;
import com.earthquake.model.Earthquake;
import com.earthquake.model.EarthquakeResponse;
import com.earthquake.ui.ErrorView;
import com.earthquake.ui.detail.MapsActivity;
import com.earthquake.utils.JsonResponseParser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG_DATA_LOADER = "TAG_DATA_LOADER";

    private List<Earthquake> earthquakes = new ArrayList<>();
    private EarthquakeListAdapter adapter = new EarthquakeListAdapter(earthquakes, earthquake -> {
        MapsActivity.open(MainActivity.this, earthquake);
    });

    private ErrorView errorView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);

        progressBar = findViewById(R.id.progressBar);
        errorView = findViewById(R.id.errorView);
        errorView.setRetryButtonListener(view -> {
            refreshData();
        });

        loadData();
    }

    private void loadData() {
        errorView.hide();
        progressBar.setVisibility(View.VISIBLE);
        getDataLoader().loadData(this::updateUi);
    }

    private void refreshData() {
        errorView.hide();
        progressBar.setVisibility(View.VISIBLE);
        getDataLoader().refreshData(this::updateUi);
    }

    private void updateUi(Response<String> response) {
        progressBar.setVisibility(View.GONE);
        if (response.isSuccessful()) {
            showData(response);
        } else {
            showErrorView(response);
        }
    }

    private void showData(Response<String> response) {
        errorView.hide();
        EarthquakeResponse earthquakeResponse =
                JsonResponseParser.toEarthquakeResponse(response.getData());
        earthquakes.clear();
        earthquakes.addAll(earthquakeResponse.getEarthquakes());
        adapter.notifyDataSetChanged();
    }

    private void showErrorView(Response<String> response) {
        errorView.show(response.getThrowable());
    }

    private EarthquakeDataLoader getDataLoader() {
        EarthquakeDataLoader dataLoader = (EarthquakeDataLoader) getSupportFragmentManager()
                .findFragmentByTag(TAG_DATA_LOADER);

        if (dataLoader == null) {
            dataLoader = new EarthquakeDataLoader();
            getSupportFragmentManager().beginTransaction()
                    .add(dataLoader, TAG_DATA_LOADER)
                    .commitNow();
        }

        return dataLoader;
    }

}
