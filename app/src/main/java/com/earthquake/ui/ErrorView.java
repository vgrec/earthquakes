package com.earthquake.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.earthquake.R;

/**
 * A simple error view with an Info text and Retry button.
 */
public class ErrorView extends LinearLayout {
    private TextView errorMessageTextView;

    public ErrorView(Context context) {
        this(context, null);
    }

    public ErrorView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ErrorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.error_view, this);

        setGravity(Gravity.CENTER);
        setOrientation(LinearLayout.VERTICAL);

        errorMessageTextView = findViewById(R.id.errorMessageTextView);
    }

    public void show(Throwable throwable) {
        setVisibility(View.VISIBLE);
        if (throwable.getMessage() != null) {
            errorMessageTextView.setText(throwable.getMessage());
        } else {
            errorMessageTextView.setText(R.string.error_view_default_text);
        }
    }

    public void hide() {
        setVisibility(View.GONE);
    }

    public void setRetryButtonListener(OnClickListener clickListener) {
        findViewById(R.id.retryButton).setOnClickListener(clickListener);
    }
}
