package com.earthquake.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.earthquake.R;
import com.earthquake.model.Earthquake;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Activity that shows the location of an earthquake on the map.
 */
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final int ZOOM_LEVEL_CONTINENT = 5;
    private static final String KEY_LATITUDE = "LATITUDE";
    private static final String KEY_LONGITUDE = "LONGITUDE";

    /**
     * Helper method to open {@link MapsActivity}.
     */
    public static void open(Context context, Earthquake earthquake) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(KEY_LATITUDE, earthquake.getLatitude());
        intent.putExtra(KEY_LONGITUDE, earthquake.getLongitude());
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = getMapFragment();
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    private SupportMapFragment getMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
    }

    /**
     * Triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        String latitude = getIntent().getStringExtra(KEY_LATITUDE);
        String longitude = getIntent().getStringExtra(KEY_LONGITUDE);
        if (latitude == null || longitude == null) {
            showErrorTextView();
            return;
        }

        LatLng coordinates = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
        googleMap.addMarker(new MarkerOptions().position(coordinates));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, ZOOM_LEVEL_CONTINENT));
    }

    private void showErrorTextView() {
        findViewById(R.id.mapsErrorTextView).setVisibility(View.VISIBLE);
    }

}
