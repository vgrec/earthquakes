package com.earthquake.http;

import com.earthquake.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class provides the ability execute {@link Request}s.
 */
public class RequestManager {
    private static final int UNKNOWN_RESPONSE_CODE = -1;
    private ExecutorService service = Executors.newFixedThreadPool(3);

    /**
     * Submit a request for execution.
     *
     * @param request the {@link Request} to be executed.
     */
    public void execute(final Request request) {
        service.submit(() -> {
            // Return early if the request was already canceled.
            if (request.isCanceled()) {
                return;
            }

            NetworkResponse networkResponse = executeGet(request.getPath());
            request.deliverResponse(networkResponse);
        });
    }

    /**
     * Stop all actively running requests.
     */
    public void shutDown() {
        service.shutdownNow();
    }

    private NetworkResponse executeGet(String path) {
        InputStream inputStream = null;
        try {
            URL url = new URL(path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                inputStream = connection.getInputStream();
                return NetworkResponse.success(toByteArray(inputStream));
            } else {
                return NetworkResponse.error(responseCode, new UnsuccessfulResponseException(
                        "Request to " + path + " returned unsuccessful response: " + responseCode));
            }
        } catch (IOException e) {
            Utils.logError("Could not execute: " + path, e);
            return NetworkResponse.error(UNKNOWN_RESPONSE_CODE, e);
        } finally {
            closeSilently(inputStream);
        }
    }


    private byte[] toByteArray(InputStream inputStream) throws IOException {
        // Source: https://www.techiedelight.com/convert-inputstream-byte-array-java/

        byte[] buffer = new byte[1024];
        int length;
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        while ((length = inputStream.read(buffer)) != -1) {
            os.write(buffer, 0, length);
        }

        return os.toByteArray();
    }

    private void closeSilently(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                Utils.logError("Exception thrown while closing the stream", e);
            }
        }
    }
}
