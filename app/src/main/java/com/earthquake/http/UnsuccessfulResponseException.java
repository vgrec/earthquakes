package com.earthquake.http;

import java.io.IOException;

/**
 * An exception type to signal that a request was unsuccessful.
 */
public class UnsuccessfulResponseException extends IOException {
    UnsuccessfulResponseException(String message) {
        super(message);
    }
}
