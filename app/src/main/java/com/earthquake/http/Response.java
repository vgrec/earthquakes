package com.earthquake.http;

import java.net.HttpURLConnection;

/**
 * Simple POJO for encapsulating an http response.
 */
public class Response<T> {
    private int code;
    private T data;
    private Throwable throwable;

    /**
     * @return the response code.
     */
    public int getCode() {
        return code;
    }

    /**
     * @return the response data.
     */
    public T getData() {
        return data;
    }

    /**
     * @return an instance of {@link Throwable} if an exception
     * was thrown.
     */
    public Throwable getThrowable() {
        return throwable;
    }

    /**
     * @return true whether the request completed successfully.
     */
    public boolean isSuccessful() {
        return code >= 200 && code <= 299;
    }

    /**
     * Helper method for creating an error response.
     *
     * @param code      the http result code
     * @param throwable the cause of the error
     * @return an instance of {@link Response}
     */
    static <T>Response<T> error(int code, Throwable throwable) {
        Response<T> response = new Response<>();
        response.code = code;
        response.throwable = throwable;
        return response;
    }

    /**
     * Helper method for creating a success response
     *
     * @param data the response data
     * @return an instance of {@link Response}
     */
    static <T> Response<T> success(T data) {
        Response<T> response = new Response<>();
        response.code = HttpURLConnection.HTTP_OK;
        response.data = data;
        return response;
    }

    /**
     * A listener to be notified when the response is ready.
     */
    public interface Listener<T> {
        void onResponseReady(Response<T> response);
    }
}