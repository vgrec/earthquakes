package com.earthquake.http;

import android.os.Handler;
import android.os.Looper;

/**
 * Base class for encapsulating an http request.
 */
abstract public class Request<T> {
    private String path;
    private Response.Listener<T> listener;
    private Handler uiHandler = new Handler(Looper.getMainLooper());

    Request(String path, Response.Listener<T> listener) {
        this.path = path;
        this.listener = listener;
    }

    /**
     * @return the path this request points to.
     */
    String getPath() {
        return path;
    }

    /**
     * "Cancels" a request.
     * <p>
     * Calling this method will set the response listener to null,
     * thus ignoring any results from this request, but it will not
     * interrupt the currently running request.
     */
    public void cancel() {
        listener = null;
    }

    /**
     * @return true if the current request was canceled.
     */
    boolean isCanceled() {
        return listener == null;
    }

    /**
     * Deliver the {@link Response} if current instance of
     * {@link Request} was not canceled.
     *
     * @param networkResponse the {@link NetworkResponse} containing the data.
     */
    void deliverResponse(NetworkResponse networkResponse) {
        if (isCanceled()) {
            return;
        }

        Response<T> response = processNetworkResponse(networkResponse);
        uiHandler.post(() -> {
            // After processing the network response check once again
            // whether the request was canceled or not, because it can
            // happen a request was canceled in the middle of the processing.
            if (!isCanceled()) {
                listener.onResponseReady(response);
            }
        });
    }

    abstract Response<T> processNetworkResponse(NetworkResponse networkResponse);
}
