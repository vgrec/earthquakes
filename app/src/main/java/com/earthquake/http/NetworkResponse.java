package com.earthquake.http;

class NetworkResponse {
    int code;
    byte[] data;
    Throwable throwable;

    boolean isSuccessful() {
        return code >= 200 && code <= 299;
    }

    static NetworkResponse success(byte[] data) {
        NetworkResponse networkResponse = new NetworkResponse();
        networkResponse.code = 200;
        networkResponse.data = data;
        return networkResponse;
    }

    static NetworkResponse error(int code, Throwable throwable) {
        NetworkResponse networkResponse = new NetworkResponse();
        networkResponse.code = code;
        networkResponse.throwable = throwable;
        return networkResponse;
    }
}
