package com.earthquake.http;

/**
 * Implementation of {@link Request} that converts the
 * network response into a {@link String}.
 */
public class StringRequest extends Request<String> {

    public StringRequest(String path, Response.Listener<String> listener) {
        super(path, listener);
    }

    @Override
    Response<String> processNetworkResponse(NetworkResponse networkResponse) {
        if (networkResponse.isSuccessful()) {
            return Response.success(new String(networkResponse.data));
        }

        return Response.error(networkResponse.code, networkResponse.throwable);
    }

}
